import styled from "styled-components";
import React, { useState } from "react";
import { COLORS } from "./styles/colors";
import { EditorComponent } from "./components/editor/editor";
import { GraphComponent } from "./components/graph/graph";
import { SIMPLE_GRAPH } from "./components/graph/simple-graph-preset";
import { ErrorBoundary } from "./components/error-boundary/error-boundary";

const App = styled.div`
  height: 100%;
  display: flex;
`;

const Editor = styled.div`
  flex: 1;
  border: 1px solid ${COLORS.Editor.Border};
`;

const Graph = styled.div`
  flex: 1;
`;

function AppComponent() {
  const GRAPH_JSON = JSON.stringify(SIMPLE_GRAPH, null, "\t");
  const [editorText, setEditorText] = useState(GRAPH_JSON);

  return (
    <App>
      <ErrorBoundary errorMessage="Something went wrong.">
        <React.StrictMode>
          <Editor>
            <EditorComponent
              initialValue={editorText}
              onChange={setEditorText}
            />
          </Editor>
        </React.StrictMode>
        <Graph>
          <ErrorBoundary
            errorMessage="Bad JSON"
            relatedErrorMessageProps={editorText}
          >
            <GraphComponent fromJson={editorText} />
          </ErrorBoundary>
        </Graph>
      </ErrorBoundary>
    </App>
  );
}

export { AppComponent };
