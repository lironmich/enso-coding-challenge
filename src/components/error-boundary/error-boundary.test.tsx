import React from "react";
import { render } from "@testing-library/react";
import { ErrorBoundary } from "./error-boundary";

describe("error-boundary", () => {
  let consoleError: any;

  describe("without errors", () => {
    test("should render the children", async () => {
      const renderResult = render(
        <ErrorBoundary errorMessage="error">children</ErrorBoundary>
      );

      const children = await renderResult.findByText("children");

      expect(children).toBeTruthy();
    });
  });

  describe("with errors", () => {
    beforeEach(() => {
      consoleError = console.error;
      console.error = () => {};
    });

    afterEach(() => {
      console.error = consoleError;
    });

    test("should render error message", async () => {
      const ErrorComponent = () => {
        throw "bad component";
      };

      const renderResult = render(
        <ErrorBoundary errorMessage="error">
          <ErrorComponent />
        </ErrorBoundary>
      );

      const children = await renderResult.findByText("error");

      expect(children).toBeTruthy();
    });
  });
});
