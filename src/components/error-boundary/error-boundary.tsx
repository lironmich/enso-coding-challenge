import React, { ReactNode } from "react";
import styled from "styled-components";

interface IErrorBoundaryProps {
  errorMessage: string;
  children: ReactNode;
  relatedErrorMessageProps?: Object;
}

interface IErrorBoundaryState {
  hasError: boolean;
}

const ErrorMessage = styled.h1`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

class ErrorBoundary extends React.Component<
  IErrorBoundaryProps,
  IErrorBoundaryState
> {
  constructor(props: IErrorBoundaryProps) {
    super(props);
    this.state = { hasError: false };
  }

  componentDidUpdate(prevProps: IErrorBoundaryProps) {
    if (
      JSON.stringify(this.props.relatedErrorMessageProps) !==
      JSON.stringify(prevProps.relatedErrorMessageProps)
    ) {
      this.setState({ hasError: false });
    }
  }

  static getDerivedStateFromError() {
    return { hasError: true };
  }

  render() {
    if (this.state.hasError) {
      return <ErrorMessage>{this.props.errorMessage}</ErrorMessage>;
    }

    return this.props.children;
  }
}

export { ErrorBoundary };
