import React from "react";
import AceEditor from "react-ace";
import styled from "styled-components";

import "ace-builds/src-noconflict/mode-json";
import "ace-builds/src-noconflict/theme-github";

const Editor = styled(AceEditor)``;

interface IEditorComponentProps {
  initialValue: string;
  onChange: (value: string) => void;
}

function EditorComponent(props: IEditorComponentProps) {
  return (
    <Editor
      mode="json"
      theme="github"
      onChange={props.onChange}
      fontSize={14}
      showGutter={true}
      highlightActiveLine={true}
      value={props.initialValue}
      width="100%"
      height="100%"
      setOptions={{ useWorker: false }}
    />
  );
}

export { EditorComponent };
