import React from "react";
import styled from "styled-components";
import CytoscapeComponent from "react-cytoscapejs";
import { ElementsDefinition } from "cytoscape";

const Graph = styled(CytoscapeComponent)`
  height: 100%;
`;

const ErrorMessage = styled.h1`
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

interface IGraphComponentProps {
  fromJson: string;
}

function GraphComponent(props: IGraphComponentProps) {
  let graph: ElementsDefinition;

  try {
    graph = JSON.parse(props.fromJson);
  } catch {
    return <ErrorMessage>Bad JSON</ErrorMessage>;
  }

  return (
    <Graph
      elements={CytoscapeComponent.normalizeElements({
        nodes: graph.nodes,
        edges: graph.edges,
      })}
    />
  );
}

export { GraphComponent };
