const COLOR_PALLETE = {
    GRAY_1: '#aaaaaa'
}

export const COLORS = {
    Editor: {
        Border: COLOR_PALLETE.GRAY_1
    }
}